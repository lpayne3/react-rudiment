var React = require('react');
var Link = require('react-router-dom').Link;

class About extends React.Component {
	render() {
	return (
		<div>
			<center><div className='home-container'>
			<h1>This web app is a remix of the Github Battle app by Tyler McGinnis taught in his React Fundamentals course.</h1> 
			
			<a href='https://tylermcginnis.com/courses/react-fundamentals/'><img src='https://camo.githubusercontent.com/81d6ae53ab31b0ff2caaf344f04f3f324c295b70/68747470733a2f2f74796c65726d6367696e6e69732e636f6d2f74796c65726d6367696e6e69735f676c61737365732d3330302e706e67' style={{width: 300, height: 300}} align="middle"/></a>
			
			<Link className='button' to='/home'>Home</Link>
			</div></center>
		</div>
		)
	}
}
module.exports = About;
