var React = require('react');
var Link = require('react-router-dom').Link;

class Home extends React.Component {
  render() {
    return (
    <div>
	<center><div className='column'>
		    <img src='https://www.aha.io/assets/github.7433692cabbfa132f34adb034e7909fa.png' style={{width: 800, height: 300}} align="middle"/>
	</div></center>
      <div className='home-container'>
        <h2>Github Search: Find your friends online!</h2>
        
	
	<Link className='button' to='/search'>GitSearch</Link>
      </div>
		 
    </div>
    )
  }
}

module.exports = Home;
